import React, { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import { Row, Col, Image } from 'react-bootstrap'
import axios from 'axios'
import Product from '../components/Product'

const AuthorScreen = () => {
	const { id } = useParams()
	const [user, setUser] = useState({})
	const [products, setProducts] = useState([])

	useEffect(() => {
		const fetchProduct = async () => {
			const { data } = await axios.get(`http://51.178.220.115/api/usersProducts/${id}`)

			setUser(data.user)
			setProducts(data.products)
		}

		fetchProduct()
	}, [user, products, id])

	return (
		<>
			<br />
			<div className='text-center'>
				<Image
					src={user.image}
					alt={user.name}
					fluid
					roundedCircle
					style={{ width: '120px', height: '120px' }}
				/>
				<br />
				<br />
				<h3>{user.name}</h3>
				<h5>email : {user.email}</h5>
				<br />
			</div>
			<hr className='pt-0 mt-0' />
			<Row>
				{products.map(
					(product) =>
						product.public && (
							<Col key={product._id} sm={12} md={6} lg={4}>
								<Product product={product} />
							</Col>
						)
				)}
			</Row>
		</>
	)
}

export default AuthorScreen
