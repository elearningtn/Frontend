import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { Image } from 'react-bootstrap'
import axios from 'axios'

const AdminAddProductScreen = () => {
	const navigate = useNavigate()

	const [user, setUser] = useState({})
	const [courcename, setCourcename] = useState('')
	const [coursedescription, setCoursedescription] = useState('')
	const [courseimage, setCourseimage] = useState([])
	const [imagePreview, setImagePreview] = useState('/images/Image-icon.png')
	const [coursepdf, setCoursepdf] = useState([])
	const [pdfPreview, setPdfPreview] = useState('')
	const [visibility, setVisibility] = useState(true)
	const [inputFields, setInputFields] = useState([{ name: '', description: '', link: '' }])

	useEffect(() => {
		if (
			!JSON.parse(localStorage.getItem('obaUserInfo')) ||
			JSON.parse(localStorage.getItem('obaUserInfo')).isAdmin === false
		) {
			navigate('/')
		}

		const fetchProduct = async () => {
			const id = JSON.parse(localStorage.getItem('obaUserInfo'))._id
			const { data } = await axios.get(`http://51.178.220.115/api/users/${id}`)

			setUser(data.item)
		}

		fetchProduct()
	}, [navigate, user])

	const handleFormChange = (index, event) => {
		let data = [...inputFields]
		data[index][event.target.name] = event.target.value
		setInputFields(data)
	}

	const addFields = () => {
		if (
			inputFields.length === 0 ||
			(inputFields[inputFields.length - 1].name !== '' &&
				inputFields[inputFields.length - 1].link !== '')
		) {
			let newfield = { name: '', description: '', link: '' }

			setInputFields([...inputFields, newfield])
		}
	}

	const removeFields = (index) => {
		if (index === 0) {
			alert('Required one lesson or more!')
		} else {
			let data = [...inputFields]
			data.splice(index, 1)
			setInputFields(data)
		}
	}

	const changeImage = (e) => {
		setCourseimage(e.target.files)

		const reader = new FileReader()

		reader.onload = () => {
			if (reader.readyState === 2) {
				setImagePreview(reader.result)
			}
		}

		reader.readAsDataURL(e.target.files[0])
	}

	const changePDF = (e) => {
		setCoursepdf(e.target.files)

		const reader = new FileReader()

		reader.onload = () => {
			if (reader.readyState === 2) {
				setPdfPreview(reader.result)
			}
		}

		reader.readAsDataURL(e.target.files[0])

		console.log(pdfPreview)
	}

	const submitHandler = async (e) => {
		e.preventDefault()

		if (courcename === '') {
			alert('Course Name is required!')
		} else {
			if (courseimage.length === 0) {
				alert('Course Image is required!')
			} else {
				if (
					inputFields[inputFields.length - 1].name === '' ||
					inputFields[inputFields.length - 1].link === ''
				) {
					alert('Lesson Name & Link is required!')
				} else {
					const formData = new FormData()
					formData.append('image', courseimage[0])

					const config = {
						headers: {
							'Content-Type': 'multipart/form-data',
						},
					}

					const { data } = await axios.post('http://51.178.220.115/api/products/upload/', formData, config)

					const config2 = {
						headers: {
							'Content-Type': 'application/json',
						},
					}

					let data2 = null
					let newpdf = null
					if (coursepdf.length !== 0) {
						const formData = new FormData()
						formData.append('file', coursepdf[0])

						const config2 = {
							headers: {
								'Content-Type': 'multipart/form-data',
							},
						}

						data2 = await axios.post('http://51.178.220.115/api/products/uploadfile/', formData, config2)
						newpdf = data2.data.image
					}

					await axios.post(
						'/api/products/',
						{
							user,
							name: courcename,
							description: coursedescription,
							image: data.image,
							pdf: newpdf,
							public: visibility,
							lessons: inputFields,
						},
						config2
					)

					navigate('/admin')
				}
			}
		}
	}

	return (
		<div className='card my-3'>
			<h5 className='card-header text-center'>Add New Course</h5>
			<div className='card-body'>
				<form encType='multipart/form-data' onSubmit={submitHandler}>
					<div className='form-group'>
						<label htmlFor='courcename' className='small'>
							Course Name :
						</label>
						<input
							type='text'
							className='form-control'
							id='courcename'
							placeholder='Enter Cource Name'
							value={courcename}
							onChange={(e) => setCourcename(e.target.value)}
						/>
					</div>
					<div className='form-group'>
						<label htmlFor='coursedescription' className='small'>
							Course Description :
						</label>
						<textarea
							className='form-control'
							id='coursedescription'
							rows='4'
							placeholder='Enter Course Description'
							value={coursedescription}
							onChange={(e) => setCoursedescription(e.target.value)}
						></textarea>
					</div>
					<div className='form-group'>
						<label htmlFor='courseimage' className='small'>
							Image
						</label>
						<div className='input-group mb-3'>
							<div className='input-group-prepend'>
								<span className='input-group-text'>
									<Image
										src={imagePreview}
										alt='error'
										fluid
										style={{ width: '25px', height: '25px' }}
									/>
								</span>
							</div>
							<div className='custom-file'>
								<input
									type='file'
									className='custom-file-input'
									id='courseimage'
									onChange={changeImage}
								/>
								<label className='custom-file-label' htmlFor='courseimage'>
									Choose file
								</label>
							</div>
						</div>
					</div>

					<div className='form-group'>
						<label htmlFor='coursepdf' className='small'>
							PDF File
						</label>
						<div className='custom-file'>
							<input
								type='file'
								className='custom-file-input'
								id='coursepdf'
								onChange={changePDF}
								accept='application/pdf, application/vnd.ms-excel'
							/>
							<label className='custom-file-label' htmlFor='coursepdf'>
								Choose file
							</label>
						</div>
					</div>

					<div className='form-group'>
						<label htmlFor='visibility' className='small'>
							Visibility
						</label>
						<select
							className='form-control'
							id='visibility'
							value={visibility}
							onChange={(e) => setVisibility(e.target.value)}
						>
							<option value={true}>Public</option>
							<option value={false}>Private</option>
						</select>
					</div>

					<br />
					<hr className='mb-0 pb-0' />
					<h5 className='card-header text-center mb-4'>Lessons</h5>
					<br />

					{inputFields.map((input, index) => {
						return (
							<div key={index} className='row'>
								<div className='form-group col-3'>
									<input
										type='text'
										className='form-control'
										name='name'
										placeholder='Lesson Name'
										value={input.name}
										onChange={(event) => handleFormChange(index, event)}
									/>
								</div>

								<div className='form-group col'>
									<input
										type='text'
										className='form-control'
										name='description'
										placeholder='Lesson Description'
										value={input.description}
										onChange={(event) => handleFormChange(index, event)}
									/>
								</div>

								<div className='form-group col-3'>
									<input
										type='url'
										className='form-control'
										name='link'
										placeholder='Lesson Link'
										value={input.link}
										onChange={(event) => handleFormChange(index, event)}
									/>
								</div>

								<div className='col-1'>
									<button
										type='button'
										className='btn btn-danger'
										onClick={() => removeFields(index)}
									>
										<i className='fas fa-remove' aria-hidden='true'></i>
									</button>
								</div>
							</div>
						)
					})}

					<div className='text-right'>
						<button type='button' className='btn btn-success' onClick={addFields}>
							<i className='fas fa-plus' aria-hidden='true'></i>
							&nbsp;&nbsp;Add New Lesson
						</button>
					</div>

					<hr />

					<div className='text-center mt-4'>
						<button type='submit' className='btn btn-primary'>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	)
}

export default AdminAddProductScreen
