import React, { useState, useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'
import Product from '../components/Product'
import axios from 'axios'

const HomeScreen = () => {
	const [products, setProducts] = useState([])

	useEffect(() => {
		const fetchProducts = async () => {
			const { data } = await axios.get('http://51.178.220.115/api/products')

			setProducts(data.items)
		}
		fetchProducts()
	}, [])

	return (
		<>
			<h1 className='my-4'>Latest Courses</h1>
			<Row>
				{products.map(
					(product) =>
						product.public && (
							<Col key={product._id} sm={12} md={6} lg={4}>
								<Product product={product} />
							</Col>
						)
				)}
			</Row>
		</>
	)
}

export default HomeScreen

