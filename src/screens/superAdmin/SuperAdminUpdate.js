import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios'

const SuperAdminUpdate = () => {
	const navigate = useNavigate()

	const { id } = useParams()

	const [user, setUser] = useState({})
	const [isAdmin, setIsAdmin] = useState(false)
	const [isSuperAdmin, setIsSuperAdmin] = useState(false)

	useEffect(() => {
		if (
			!JSON.parse(localStorage.getItem('obaUserInfo')) ||
			JSON.parse(localStorage.getItem('obaUserInfo')).isSuperAdmin === false
		) {
			navigate('/')
		}

		const fetchUser = async () => {
			const { data } = await axios.get(`http://51.178.220.115/api/users/${id}`)

			setUser(data.item)

			setIsAdmin(data.item.isAdmin)
			setIsSuperAdmin(data.item.isSuperAdmin)
		}

		fetchUser()
	}, [id, navigate])

	const submitHandler = async (e) => {
		e.preventDefault()

		const config2 = {
			headers: {
				'Content-Type': 'application/json',
			},
		}

		await axios.put(
			`http://51.178.220.115/api/users/${id}`,
			{
				isAdmin,
				isSuperAdmin,
			},
			config2
		)

		navigate('/superadmin')
	}

	return (
		<div className='card my-3'>
			<h5 className='card-header text-center'>Update User</h5>
			<div className='card-body'>
				<form encType='multipart/form-data' onSubmit={submitHandler}>
					<div className='form-group'>
						<label htmlFor='username' className='small'>
							Name :
						</label>
						<input
							type='text'
							className='form-control'
							id='username'
							placeholder='Enter Cource Name'
							disabled
							value={user.name}
						/>
					</div>

					<div className='form-group'>
						<label htmlFor='useremail' className='small'>
							Email :
						</label>
						<input
							type='text'
							className='form-control'
							id='useremail'
							placeholder='Enter Cource Name'
							disabled
							value={user.email}
						/>
					</div>

					<div className='form-group'>
						<label htmlFor='isadmin' className='small'>
							is Admin
						</label>
						<select
							className='form-control'
							id='isadmin'
							value={isAdmin}
							onChange={(e) => setIsAdmin(e.target.value)}
						>
							<option value={true}>True</option>
							<option value={false}>False</option>
						</select>
					</div>

					<div className='form-group'>
						<label htmlFor='superadmin' className='small'>
							is SuperAdmin
						</label>
						<select
							className='form-control'
							id='superadmin'
							value={isSuperAdmin}
							onChange={(e) => setIsSuperAdmin(e.target.value)}
						>
							<option value={true}>True</option>
							<option value={false}>False</option>
						</select>
					</div>

					<div className='text-center mt-4'>
						<button type='submit' className='btn btn-primary'>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	)
}

export default SuperAdminUpdate
