import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Image } from 'react-bootstrap'
import axios from 'axios'

const SuperAdminScreen = () => {
	const navigate = useNavigate()

	const [users, setUsers] = useState([])

	useEffect(() => {
		if (
			!JSON.parse(localStorage.getItem('obaUserInfo')) ||
			JSON.parse(localStorage.getItem('obaUserInfo')).isSuperAdmin === false
		) {
			navigate('/')
		}

		const fetchProduct = async () => {
			// const id = JSON.parse(localStorage.getItem('obaUserInfo'))._id
			const { data } = await axios.get(`http://51.178.220.115/api/users`)

			setUsers(data.items)
		}

		fetchProduct()
	}, [navigate, users])

	const deleteHandler = (id) => {
		if (window.confirm('Delete the item?')) {
			axios.delete(`http://51.178.220.115/api/users/${id}`)
		}
	}

	return (
		<>
			<br />
			<div className='table-responsive'>
				<table className='table text-center'>
					<thead className='thead-dark'>
						<tr>
							<th scope='col'>ID</th>
							<th scope='col'>Image</th>
							<th scope='col'>Name</th>
							<th scope='col'>Action</th>
						</tr>
					</thead>
					<tbody>
						{users &&
							users.map((user) => (
								<tr key={user._id}>
									<td>{user._id}</td>
									<td>
										<Image
											src={user.image}
											alt={user.name}
											fluid
											roundedCircle
											style={{ width: '25px', height: '25px' }}
										/>
									</td>
									<td>{user.name}</td>
									<td className='text-nowrap'>
										<Link
											to={`/superadmin/update/${user._id}`}
											className='btn btn-link text-warning px-2 py-0 mx-1'
										>
											<i className='fas fa-pen' aria-hidden='true'></i>
										</Link>

										<button
											type='button'
											className='btn btn-link text-danger px-2 py-0'
											onClick={() => deleteHandler(user._id)}
										>
											<i className='fas fa-trash' aria-hidden='true'></i>
										</button>
									</td>
								</tr>
							))}
					</tbody>
				</table>
			</div>
		</>
	)
}

export default SuperAdminScreen
