import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Row, Col, Image, ListGroup } from 'react-bootstrap'
import axios from 'axios'
import { useSelector } from 'react-redux'

const CouponScreen = () => {
	const navigate = useNavigate()
	const [product, setProduct] = useState({})
	const [url, setUrl] = useState('')
	const [coupon, setCoupon] = useState('')
	const [visibility, setVisibility] = useState(false)

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	useEffect(() => {
		if (!userInfo) {
			navigate('/')
		}
	}, [navigate, userInfo])

	const submitHandler = async (e) => {
		e.preventDefault()

		if (coupon) {
			const { data } = await axios.get(`http://51.178.220.115/api/products/${coupon}`)

			setUrl(data.item.lessons[0].link)

			setProduct(data.item)

			setVisibility(true)
		} else {
			alert('Coupon required!')
		}
	}

	const onChangeVisibility = (e) => {
		setVisibility(false)
		setCoupon(e.target.value)
	}

	return (
		<>
			<form encType='multipart/form-data' onSubmit={submitHandler}>
				<div className='form-group'>
					<label htmlFor='username' className='small'>
						Coupon :
					</label>
					<input
						type='text'
						className='form-control'
						id='username'
						placeholder='Enter Cource Coupon'
						value={coupon}
						onChange={onChangeVisibility}
					/>
				</div>

				<div className='text-center my-4'>
					<button className='btn btn-success' onClick={submitHandler}>
						Submit
					</button>
				</div>
			</form>

			<hr />

			{coupon && visibility && (
				<>
					<br />
					<Row>
						<Col md={6}>
							<Image
								src={product.image}
								alt={product.name}
								fluid
								style={{ width: '100%' }}
							/>
						</Col>
						<Col md={6}>
							<ListGroup variant='flush'>
								<ListGroup.Item>
									<h3 className='pt-0 pb-1'>{product.name}</h3>
								</ListGroup.Item>

								<ListGroup.Item>
									<Image
										src={product.authorImage}
										alt={product.authorName}
										fluid
										roundedCircle
										style={{ width: '25px', height: '25px' }}
									/>
									&nbsp;&nbsp;
									<Link to={`/author/${product.user}`}>
										<small>{product.authorName}</small>
									</Link>
								</ListGroup.Item>

								<ListGroup.Item>
									<strong>Description: </strong>
									{product.description}
								</ListGroup.Item>
							</ListGroup>
						</Col>
					</Row>
					<hr />
					<br />

					<div className='row'>
						<div className='col-6'>
							<iframe
								src={url}
								frameBorder='0'
								allow='autoplay; encrypted-media'
								allowFullScreen
								className='w-100'
								style={{ minHeight: '300px' }}
								title='video'
							/>{' '}
						</div>
						<div className='col-6'>
							{product &&
								product.lessons &&
								product.lessons.map((lesson, index) => (
									<span
										key={index}
										style={{ cursor: 'pointer' }}
										onClick={() => setUrl(lesson.link)}
									>
										<h5>{lesson.name}</h5>
										<p>{lesson.description}</p>
										<hr className='py-0 my-0 mb-3' />
									</span>
								))}
						</div>
					</div>
				</>
			)}
		</>
	)
}

export default CouponScreen
