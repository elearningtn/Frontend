import React, { useState, useEffect } from 'react'
import { Link, useParams, useNavigate } from 'react-router-dom'
import { Row, Col, Image, ListGroup } from 'react-bootstrap'
import axios from 'axios'
import { useSelector } from 'react-redux'
import packageJson from '../../package.json'

const ProductScreen = () => {
	const navigate = useNavigate()
	const { id } = useParams()
	const [product, setProduct] = useState({})
	const [url, setUrl] = useState('')

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const proxy = packageJson.proxy

	useEffect(() => {
		const fetchProduct = async () => {
			const { data } = await axios.get(`http://51.178.220.115:4200/api/products/${id}`)

			setUrl(data.item.lessons[0].link)

			setProduct(data.item)
		}

		fetchProduct()
	}, [id])

	return (
		<>
			{product.public || (userInfo && product.user === userInfo._id) ? (
				<>
					<br />
					<Row>
						<Col md={6}>
							<Image
								src={product.image}
								alt={product.name}
								fluid
								style={{ width: '100%' }}
							/>
						</Col>
						<Col md={6}>
							<ListGroup variant='flush'>
								<ListGroup.Item>
									<h3 className='pt-0 pb-1'>{product.name}</h3>
								</ListGroup.Item>

								<ListGroup.Item>
									<Image
										src={product.authorImage}
										alt={product.authorName}
										fluid
										roundedCircle
										style={{ width: '25px', height: '25px' }}
									/>
									&nbsp;&nbsp;
									<Link to={`http://51.178.220.115:4200/author/${product.user}`}>
										<small>{product.authorName}</small>
									</Link>
								</ListGroup.Item>

								<ListGroup.Item>
									<strong>Description: </strong>
									{product.description}
								</ListGroup.Item>
							</ListGroup>
						</Col>
					</Row>
					<hr />

					{product && product.pdf && (
						<div className='text-center'>
							<a href={proxy + product.pdf} target='blank' className='btn btn-dark'>
								Download PDF
							</a>
							<hr />
						</div>
					)}

					<br />

					<div className='row'>
						<div className='col-6'>
							<iframe
								src={url}
								frameBorder='0'
								allow='autoplay; encrypted-media'
								allowFullScreen
								className='w-100'
								style={{ minHeight: '300px' }}
								title='video'
							/>{' '}
						</div>
						<div className='col-6'>
							{product &&
								product.lessons &&
								product.lessons.map((lesson, index) => (
									<span
										key={index}
										style={{ cursor: 'pointer' }}
										onClick={() => setUrl(lesson.link)}
									>
										<h5>{lesson.name}</h5>
										<p>{lesson.description}</p>
										<hr className='py-0 my-0 mb-3' />
									</span>
								))}
						</div>
					</div>
				</>
			) : (
				<>
					<div className='alert alert-danger my-3 text-center' role='alert'>
						You dont have access
					</div>

					<div className='text-center'>
						<button
							type='button'
							className='btn btn-success'
							onClick={() => navigate('http://51.178.220.115:4200/')}
						>
							GO HOME!
						</button>
					</div>
				</>
			)}
		</>
	)
}

export default ProductScreen
