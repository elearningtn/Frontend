import React from 'react'
import { Link } from 'react-router-dom'
import { Card, Image } from 'react-bootstrap'

const Product = ({ product }) => {
	return (
		<Card className='my-3 p-3 rounded h-100'>
			<Link to={`/product/${product._id}`} className='h-100'>
				<Card.Img src={product.image} variant='top' className='h-100' />
			</Link>

			<Card.Body>
				<Link to={`/product/${product._id}`}>
					<Card.Title as='div'>
						<strong>{product.name}</strong>
					</Card.Title>
				</Link>
			</Card.Body>

			<Card.Footer>
				<Image
					src={product.authorImage}
					alt={product.authorName}
					fluid
					roundedCircle
					style={{ width: '25px', height: '25px' }}
				/>
				&nbsp;&nbsp;
				<Link to={`/author/${product.user}`}>
					<small>{product.authorName}</small>
				</Link>
			</Card.Footer>
		</Card>
	)
}

export default Product
