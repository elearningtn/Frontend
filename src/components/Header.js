import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { logout } from '../actions/userActions'

const Header = () => {
	const dispatch = useDispatch()

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const logoutHandler = () => {
		dispatch(logout())
	}

	return (
		<header>
			<Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect>
				<Container>
					<LinkContainer to='/'>
						<Navbar.Brand> Welcome to ElearningTN  avec ci/cd  </Navbar.Brand>
					</LinkContainer>
					<Navbar.Toggle aria-controls='basic-navbar-nav' />
					<Navbar.Collapse id='basic-navbar-nav'>
						<Nav className='ml-auto'>
							{userInfo ? (
								<NavDropdown title={userInfo.name} id='menu'>
									{userInfo.isSuperAdmin && (
										<LinkContainer to='/superadmin'>
											<NavDropdown.Item>Dashboard</NavDropdown.Item>
										</LinkContainer>
									)}

									{userInfo.isAdmin && (
										<LinkContainer to='/admin'>
											<NavDropdown.Item>Cources</NavDropdown.Item>
										</LinkContainer>
									)}

									{!userInfo.isAdmin && !userInfo.isSuperAdmin && (
										<LinkContainer to='/coupon'>
											<NavDropdown.Item>Coupon</NavDropdown.Item>
										</LinkContainer>
									)}

									<NavDropdown.Item onClick={logoutHandler}>
										Logout
									</NavDropdown.Item>
								</NavDropdown>
							) : (
								<LinkContainer to='/login'>
									<Nav.Link>
										<i className='fas fa-user'></i> Sign in
									</Nav.Link>
								</LinkContainer>
							)}
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</header>
	)
}

export default Header
