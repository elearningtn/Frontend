import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import Header from './components/Header'
import Footer from './components/Footer'
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import HomeScreen from './screens/HomeScreen'
import ProductScreen from './screens/ProductScreen'
import AuthorScreen from './screens/AuthorScreen'
import AdminProductsScreen from './screens/admin/AdminProductsScreen'
import AdminAddProductScreen from './screens/admin/AdminAddProductScreen'
import AdminUpdateProductScreen from './screens/admin/AdminUpdateProductScreen'
import SuperAdminScreen from './screens/superAdmin/SuperAdminScreen'
import SuperAdminUpdate from './screens/superAdmin/SuperAdminUpdate'
import CouponScreen from './screens/CouponScreen'

const App = () => {
	return (
		<Router>
			<Header />
			<main className='py-3'>
				<Container>
					<Routes>
						<Route path='/login' element={<LoginScreen />} />
						<Route path='/register' element={<RegisterScreen />} />

						<Route path='/' element={<HomeScreen />} exact />
						<Route path='/product/:id' element={<ProductScreen />} />
						<Route path='/author/:id' element={<AuthorScreen />} />

						<Route path='/admin' element={<AdminProductsScreen />} />
						<Route path='/admin/add' element={<AdminAddProductScreen />} />
						<Route path='/admin/update/:id' element={<AdminUpdateProductScreen />} />

						<Route path='/superadmin' element={<SuperAdminScreen />} />
						<Route path='/superadmin/update/:id' element={<SuperAdminUpdate />} />

						<Route path='/coupon' element={<CouponScreen />} />
					</Routes>
				</Container>
			</main>
			<Footer />
		</Router>
	)
}

export default App
